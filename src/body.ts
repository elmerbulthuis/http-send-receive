export interface HttpSendReceiveBodyFactory {
    (signal?: AbortSignal): AsyncIterable<Uint8Array>
}

