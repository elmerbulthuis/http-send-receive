import { Builder, WebDriver } from "selenium-webdriver";

export interface BrowserContextOptions {
    browser: string,
}
export interface BrowserContext {
    driver: WebDriver
}
export async function withBrowserContext<T = void>(
    options: BrowserContextOptions,
    job: (context: BrowserContext) => Promise<T>,
): Promise<T> {
    const driver = await new Builder().
        forBrowser(options.browser).
        build();

    try {
        const result = await job({
            driver,
        });
        return result;
    }
    finally {
        await driver.quit();
    }

}

