import delay from "delay";
import * as fs from "fs";
import * as http2 from "http2";
import { second } from "msecs";

const baseUrl = new URL("https://localhost:8080/");

const http2Server = http2.createSecureServer({
    ca: fs.readFileSync("./tls/ca.pem", "utf8"),
    cert: fs.readFileSync("./tls/cert.pem", "utf8"),
    key: fs.readFileSync("./tls/key.pem", "utf8"),
}, async (req, res) => {

    const chunks = new Array<Uint8Array>;

    for await (const chunk of req) {
        chunks.push(chunk);
        await delay(1 * second);
    }

    for (const chunk of chunks) {
        await new Promise<void>(
            (resolve, reject) => res.write(
                chunk,
                error => error == null ? reject(error) : resolve(),
            ),
        );
        await delay(1 * second);
    }
    await new Promise<void>(
        resolve => res.end(
            () => resolve(),
        ),
    );
});

http2Server.listen(baseUrl.port, () => {
    process.send?.("listening");
});
