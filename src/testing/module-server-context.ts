import * as assert from "assert";
import * as http from "http";
import * as rollup from "rollup";

export interface ModuleServerContextOptions {
    alias: string,
    input: string,
}
export interface ModuleServerContext {
    url: URL,
}
export async function withModuleServerContext<T = void>(
    options: ModuleServerContextOptions,
    job: (context: ModuleServerContext) => Promise<T>,
) {
    const { input, alias } = options;
    const baseUrl = new URL("http://localhost:9090/");

    const httpServer = http.createServer(requestListener);
    try {
        await new Promise<void>(
            resolve => httpServer.listen(baseUrl.port, () => resolve()),
        );

        const result = await job({
            url: baseUrl,
        });

        return result;
    }
    finally {
        await new Promise<void>(
            (resolve, reject) => httpServer.close(error => error ? reject(error) : resolve()),
        );
    }

    async function requestListener(req: http.IncomingMessage, res: http.ServerResponse) {
        const bundle = await rollup.rollup({
            input,
        });

        res.write(`
<!DOCTYPE html>
<head>
`);
        try {
            const output = await bundle.generate({
                name: alias,
                format: "iife",
            });

            for (const o of output.output) {
                assert.ok(o.type === "chunk");

                res.write(`
<script>
${o.code}
</script>
`);
            }

        }
        finally {
            await bundle.close();

            res.write(`
</head>
<body>
</body>
</html>
`);
            res.end();
        }
    }
}
