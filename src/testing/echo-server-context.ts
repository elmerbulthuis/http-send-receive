import assert from "assert";
import { ChildProcess, fork } from "child_process";
import * as path from "path";

export interface EchoServerContext {
    startServer(): Promise<URL>;
    stopServer(): Promise<void>;
}
export async function withEchoServerContext<T = void>(
    type: "http" | "https" | "h2c" | "h2",
    job: (
        context: EchoServerContext,
    ) => Promise<T>,
) {
    let serverProcess: ChildProcess | undefined;

    const startServer = async () => {
        const dirname = path.dirname(new URL(import.meta.url).pathname);
        const modulePath = path.resolve(dirname, "echo-server.js");

        serverProcess = fork(
            modulePath,
            [
                type,
            ],
        );

        const baseUrl = await new Promise<URL>((resolve, reject) =>
            serverProcess?.
                addListener("message", ([message, baseUrlString]: [string, string]) => {
                    if (message === "listening") {
                        resolve(new URL(baseUrlString));
                    }
                }).
                addListener("error", reject),
        );

        return baseUrl;
    };

    const stopServer = async () => {
        assert(serverProcess);
        serverProcess.kill("SIGKILL");
        await new Promise<void>((resolve, reject) =>
            serverProcess?.
                addListener("exit", () => resolve()).
                addListener("error", reject),
        );
    };

    const result = await job({
        stopServer,
        startServer,
    });

    return result;
}
