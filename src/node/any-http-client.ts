import { AbortError } from "abort-tools";
import assert from "assert";
import * as http from "http";
import * as http2 from "http2";
import * as https from "https";
import * as net from "net";
import { Writable } from "stream";
import * as tls from "tls";
import { hasHeaderValue, HttpSendReceiveHeaders } from "../headers.js";
import { HttpReceiveResponse } from "../receive-response.js";
import { HttpSendRequest } from "../send-request.js";
import { bindEvent, ConnectTimeoutError, IdleTimeoutError, PingFailedError, waitForError, waitForEvent, withAbort } from "../utils/index.js";

export interface AnyHttpClientOptions {
    origin: URL,
    assumeHttp2?: boolean
    ca?: string,
    cert?: string,
    key?: string,
    lookup?: net.LookupFunction | undefined;
    pingInterval?: number,
    connectTimeout?: number,
    idleTimeout?: number,
    onError: (error: unknown) => void
}

export class AnyHttpClient {

    constructor(
        private readonly options: AnyHttpClientOptions,
    ) {
    }

    public destroy() {
        this.http2Session?.close();
    }

    //#region socket

    private async createSocket() {
        const {
            origin,
            ca, cert, key,
            lookup,
            connectTimeout,
            onError,
        } = this.options;

        let socket: net.Socket;

        switch (origin.protocol) {
            case "https:":
                socket = tls.connect({
                    host: origin.hostname,
                    servername: origin.hostname,
                    port: origin.port === "" ? 443 : Number(origin.port),
                    ca, cert, key,
                    lookup,
                    ALPNProtocols: ["h2", "http/1.1"],
                    timeout: connectTimeout,
                });

                assert(socket.connecting);

                await withAbort(signal => {
                    bindEvent(
                        signal,
                        socket,
                        "timeout",
                        () => socket.destroy(new ConnectTimeoutError(
                            origin,
                        )),
                    );

                    return Promise.race([
                        waitForError(signal, socket),
                        Promise.all([
                            waitForEvent(signal, socket, "connect"),
                            waitForEvent(signal, socket, "secureConnect"),
                        ]),
                    ]);
                });

                assert(!socket.connecting);
                break;

            case "http:":
                socket = net.connect({
                    host: origin.hostname,
                    port: origin.port === "" ? 80 : Number(origin.port),
                    lookup,
                    timeout: connectTimeout,
                });

                assert(socket.connecting);

                await withAbort(signal => {
                    bindEvent(
                        signal,
                        socket,
                        "timeout",
                        () => socket.destroy(new ConnectTimeoutError(
                            origin,
                        )),
                    );

                    return Promise.race([
                        waitForError(signal, socket),
                        waitForEvent(signal, socket, "connect"),
                    ]);
                });

                assert(!socket.connecting);
                break;

            default: assert.fail(`protocol ${origin.protocol} not supported`);
        }

        socket.addListener("error", onError);

        return socket;
    }

    //#endregion

    //#region http2 session

    private http2Session?: http2.ClientHttp2Session;

    private takeHttp2Session(socket: net.Socket) {
        if (this.http2Session != null) {
            return this.http2Session;
        }

        this.http2Session = this.createHttp2Session(socket);
        this.http2Session.addListener(
            "close",
            () => this.http2Session = undefined,
        );
        return this.http2Session;
    }

    private createHttp2Session(socket: net.Socket) {
        const {
            idleTimeout,
            pingInterval,
            origin,
            onError,
        } = this.options;

        const session = http2.connect(origin, {
            createConnection: () => socket,
        });

        session.addListener("error", onError);

        session.setTimeout(idleTimeout ?? 0);
        session.addListener(
            "timeout",
            () => session.destroy(new IdleTimeoutError(
                origin,
            )),
        );

        if (pingInterval != null) {
            const ping = () => {
                const pingSent = session.ping(error => {
                    if (error) {
                        session.destroy(error);
                    }
                });

                if (!pingSent) {
                    session.destroy(new PingFailedError(origin));
                }
            };

            const pingIntervalHandle = setInterval(
                ping,
                pingInterval,
            );

            session.addListener(
                "close",
                () => clearInterval(pingIntervalHandle),
            );
        }

        return session;
    }

    //#endregion

    //#region fetch

    private async fetchHttp2(
        session: http2.ClientHttp2Session,
        sendRequest: HttpSendRequest,
    ) {
        const path = sendRequest.url.pathname + sendRequest.url.search;
        const { method } = sendRequest;

        const expect100 = hasHeaderValue(sendRequest.headers, "expect", "100-continue");

        const stream = session.request(
            {
                ...sendRequest.headers,
                ":path": path,
                ":method": method,
            },
            {
                endStream: sendRequest.body == null,
            },
        );

        const responseHeaders = await withAbort(async signal => {
            const streamErrorPromise = waitForError(signal, stream);
            const responseHeadersPromise = waitForEvent<http2.IncomingHttpHeaders & http2.IncomingHttpStatusHeader>(signal, stream, "response");

            if (expect100) {
                const continuePromise = waitForEvent<void>(signal, stream, "continue");

                const responseHeaders = await Promise.race([
                    streamErrorPromise,
                    continuePromise,
                    responseHeadersPromise,
                ]);
                if (responseHeaders != null) {
                    return responseHeaders;
                }
            }

            if (sendRequest.body != null) {
                const responseHeaders = await Promise.race([
                    streamErrorPromise,
                    responseHeadersPromise,
                    writeBody(sendRequest.body(signal), stream),
                ]);
                if (responseHeaders != null) {
                    return responseHeaders;
                }
            }

            {
                const responseHeaders = await Promise.race([
                    streamErrorPromise,
                    responseHeadersPromise,
                ]);
                return responseHeaders;
            }
        });

        const receiveResponse: HttpReceiveResponse = {
            status: responseHeaders[":status"] ?? 0,
            /*
            we need the cast here, because res.headers is of type IncomingHttpHeaders
            which extends NodeJS.Dict, which has values that can be undefined!
            */
            headers: responseHeaders as HttpSendReceiveHeaders,
            /**/
            async *body(signal) {
                if (signal?.aborted) {
                    throw new AbortError("http response aborted");
                }

                const onAbort = () => stream.destroy(new AbortError("http response aborted"));
                try {
                    signal?.addEventListener("abort", onAbort);
                    yield* stream;
                }
                finally {
                    signal?.removeEventListener("abort", onAbort);
                }

            },
        };

        return receiveResponse;
    }

    private async fetchHttp1(
        socket: net.Socket,
        sendRequest: HttpSendRequest,
    ) {
        const { idleTimeout } = this.options;
        const { url, method } = sendRequest;

        const expect100 = hasHeaderValue(sendRequest.headers, "expect", "100-continue");

        const request = (socket instanceof tls.TLSSocket ? https.request : http.request)(
            url,
            {
                createConnection: () => socket,
                method,
                headers: sendRequest.headers,
            },
        );
        request.setTimeout(idleTimeout ?? 0);
        request.addListener(
            "timeout",
            () => socket.destroy(new IdleTimeoutError(
                url,
            )),
        );
        if (sendRequest.body == null) {
            request.end();
        }

        const response = await withAbort(async signal => {
            const requestErrorPromise = waitForError(signal, request);
            const responsePromise = waitForEvent<http.IncomingMessage>(signal, request, "response");

            if (expect100) {
                const continuePromise = waitForEvent<void>(signal, request, "continue");

                const response = await Promise.race([
                    requestErrorPromise,
                    continuePromise,
                    responsePromise,
                ]);
                if (response != null) {
                    return response;
                }
            }

            if (sendRequest.body != null) {
                const response = await Promise.race([
                    requestErrorPromise,
                    responsePromise,
                    writeBody(sendRequest.body(signal), request),
                ]);
                if (response != null) {
                    return response;
                }
            }

            {
                const response = await Promise.race([
                    requestErrorPromise,
                    responsePromise,
                ]);
                return response;
            }
        });

        {
            const onError = (error: Error) => response.destroy(error);
            request.addListener("error", onError);
            response.addListener(
                "close",
                () => request.removeListener("error", onError),
            );
        }

        assert(socket === response.socket);

        if (response.statusCode === 101) {
            const session = this.takeHttp2Session(socket);

            return this.fetchHttp2(session, sendRequest);
        }

        const receiveResponse: HttpReceiveResponse = {
            status: response.statusCode ?? 0,
            /*
            we need the cast here, because res.headers is of type IncomingHttpHeaders
            which extends NodeJS.Dict, which has values that can be undefined!
            */
            headers: response.headers as HttpSendReceiveHeaders,
            /**/
            async * body(signal) {
                if (signal?.aborted) {
                    throw new AbortError("http response aborted");
                }

                const onAbort = () => request.destroy(new AbortError("http response aborted"));
                try {
                    signal?.addEventListener("abort", onAbort);
                    yield* response;
                }
                finally {
                    signal?.removeEventListener("abort", onAbort);
                }
            },
        };

        return receiveResponse;
    }

    public async fetch(
        sendRequest: HttpSendRequest,
    ): Promise<HttpReceiveResponse> {
        const { assumeHttp2 } = this.options;

        if (this.http2Session) {
            return this.fetchHttp2(this.http2Session, sendRequest);
        }

        const socket = await this.createSocket();

        if (socket instanceof tls.TLSSocket) {
            if (socket.alpnProtocol === "h2") {
                const session = this.takeHttp2Session(socket);

                return this.fetchHttp2(session, sendRequest);
            }

            return this.fetchHttp1(socket, sendRequest);
        }
        else {
            if (assumeHttp2 ?? false) {
                const session = this.takeHttp2Session(socket);

                return this.fetchHttp2(session, sendRequest);
            }

            return this.fetchHttp1(socket, sendRequest);
        }
    }

    //#endregion
}

//#region helpers

async function writeBody(
    body: AsyncIterable<Uint8Array>,
    req: Writable,
) {
    for await (const chunk of body) {
        await new Promise<void>((resolve, reject) => {
            req.write(
                chunk,
                error => error == null ? resolve() : reject(error),
            );
        });
    }

    await new Promise<void>(resolve => {
        req.end(
            () => resolve(),
        );
    });
}

//#endregion

