import { isAbortError } from "abort-tools";
import assert from "assert";
import delay from "delay";
import * as fs from "fs";
import * as http from "http";
import * as https from "https";
import { second } from "msecs";
import test from "tape-promise/tape.js";
import { HttpSendReceive } from "../send-receive.js";
import { withEchoServerContext } from "../testing/index.js";
import { withAbort } from "../utils/index.js";
import { createHttpAgentSendReceive } from "./send-receive-agent.js";
import { createHttpAnySendReceive } from "./send-receive-http-any.js";

const clients = [
    "agent http",
    "agent https",
    "http",
    "https",
    "h2c",
    "h2",
];

for (const client of clients) test(
    `${client} client should error when requesting stopped server`,
    t => withContext({ client }, async context => {
        const baseUrl = await context.startServer();
        await context.stopServer();

        try {
            await context.httpSendReceive({
                method: "POST",
                url: baseUrl,
                headers: {},
                async *body(signal) {
                    while (!signal?.aborted) {
                        const encoder = new TextEncoder();
                        yield encoder.encode("x\n");
                        await delay(1 * second, { signal });
                    }
                },
            });

            t.fail();
        }
        catch (error: unknown) {
            assert(error instanceof Error && "code" in error);
            t.equal(error.code, "ECONNREFUSED");
        }
    }),

);

for (const client of clients) test(
    `${client} client should error when sending data and killing server`,
    t => withContext({ client }, async context => {
        const baseUrl = await context.startServer();
        const stopPromise = (async () => {
            await delay(2 * second);
            await context.stopServer();
        })();

        try {
            await context.httpSendReceive({
                method: "POST",
                url: baseUrl,
                headers: {},
                async *body(signal) {
                    while (!signal?.aborted) {
                        const encoder = new TextEncoder();
                        yield encoder.encode("x\n");
                        await delay(1 * second, { signal });
                    }
                },
            });

            t.fail();
        }
        catch (error: unknown) {
            assert(error instanceof Error && "code" in error);
            if (client === "h2" || client === "h2c") {
                t.equal(error.code, "ERR_STREAM_WRITE_AFTER_END");
            }
            else {
                t.equal(error.code, "ECONNRESET");
            }
        }

        await stopPromise;
    }),

);

for (const client of clients) test(
    `${client} client should error when receiving data and killing server`,
    t => withContext({ client }, async context => {
        const baseUrl = await context.startServer();
        const stopPromise = (async () => {
            await delay(2 * second);
            await context.stopServer();
        })();

        const response = await context.httpSendReceive({
            method: "POST",
            url: baseUrl,
            headers: {},
            async *body(signal) {
                const encoder = new TextEncoder();
                yield encoder.encode("1\n");
                yield encoder.encode("2\n");
                yield encoder.encode("3\n");
                yield encoder.encode("4\n");
            },
        });

        const readPromise = (async () => {
            for await (const chunk of response.body()) {
                t.comment(chunk.toString());
            }
        })();

        try {
            await readPromise;
            if (client === "h2" || client === "h2c") {
                t.pass();
            }
            else {
                t.fail();
            }
        }
        catch (error: unknown) {
            assert(error instanceof Error && "code" in error);
            if (client === "h2" || client === "h2c") {
                t.fail();
            }
            else {
                t.equal(error.code, "ECONNRESET");
            }
        }

        await stopPromise;
    }),
);

for (const client of clients) test(
    `${client} client should error when aborting request`,
    t => withContext({ client }, async context => {
        const baseUrl = await context.startServer();
        try {
            const abortController = new AbortController();
            setTimeout(() => abortController.abort(), 2 * second);

            const response = await context.httpSendReceive(
                {
                    method: "POST",
                    url: baseUrl,
                    headers: {},
                    async *body() {
                        const encoder = new TextEncoder();
                        yield encoder.encode("1\n");
                        yield encoder.encode("2\n");
                        yield encoder.encode("3\n");
                        yield encoder.encode("4\n");
                    },
                },
            );

            try {
                const body = response.body(abortController.signal);
                for await (const chunk of body) {
                    t.comment(chunk.toString());
                }
                t.fail();
            }
            catch (error) {
                t.ok(isAbortError(error), "should be aborted");
            }
        }
        finally {
            await context.stopServer();
        }
    }),
);

for (const client of clients) test(
    `${client} client should send body with expect 100`,
    t => withContext({ client }, async context => {
        const baseUrl = await context.startServer();
        try {
            const response = await context.httpSendReceive(
                {
                    method: "POST",
                    url: baseUrl,
                    headers: {
                        "expect": "100-continue",
                    },
                    async *body() {
                        //
                    },
                },
            );

            for await (const chunk of response.body()) {
                t.comment(chunk.toString());
            }
            t.pass();
        }
        finally {
            await context.stopServer();
        }
    }),
);

for (const client of clients) test(
    `${client} client should send body`,
    t => withContext({ client }, async context => {
        const baseUrl = await context.startServer();
        try {
            const response = await context.httpSendReceive(
                {
                    method: "POST",
                    url: baseUrl,
                    headers: {},
                    async *body() {
                        //
                    },
                },
            );

            for await (const chunk of response.body()) {
                t.comment(chunk.toString());
            }
            t.pass();
        }
        finally {
            await context.stopServer();
        }
    }),
);

interface ContextOptions {
    client: string;
}

interface Context {
    startServer(): Promise<URL>;
    stopServer(): Promise<void>;
    httpSendReceive: HttpSendReceive;
}
async function withContext<T = void>(options: ContextOptions, job: (
    context: Context,
) => Promise<T>) {
    switch (options.client) {
        case "agent http": {
            return withEchoServerContext(
                "http",
                context => withAbort(async signal => {
                    const agent = new http.Agent();
                    signal.addEventListener("abort", () => agent.destroy());

                    const httpSendReceive = createHttpAgentSendReceive({
                        agent,
                    });

                    await job({
                        ...context,
                        httpSendReceive,
                    });
                }),
            );
        }

        case "agent https": {
            return withEchoServerContext(
                "https",
                context => withAbort(async signal => {
                    const agent = new https.Agent({
                        ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                        cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                        key: fs.readFileSync("./tls/key.pem", "utf8"),
                    });
                    signal.addEventListener("abort", () => agent.destroy());

                    const httpSendReceive = createHttpAgentSendReceive({
                        agent,
                    });

                    await job({
                        ...context,
                        httpSendReceive,
                    });
                }),
            );
        }

        case "http":
        case "https":
        case "h2c":
        case "h2": {
            return withEchoServerContext(
                options.client,
                context => withAbort(async signal => {
                    const httpSendReceive = createHttpAnySendReceive({
                        assumeHttp2: options.client === "h2c",
                        ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                        cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                        key: fs.readFileSync("./tls/key.pem", "utf8"),
                        connectTimeout: 1 * second,
                        idleTimeout: 2 * second,
                        onError(error) {
                            //
                        },
                    });

                    await job({
                        ...context,
                        httpSendReceive,
                    });
                }),
            );
        }

    }

}
