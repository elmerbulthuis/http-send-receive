import { InstanceMemoizer } from "instance-memoizer";
import * as net from "net";
import { HttpReceiveResponse } from "../receive-response.js";
import { HttpSendReceive } from "../send-receive.js";
import { HttpSendRequest } from "../send-request.js";
import { AnyHttpClient } from "./any-http-client.js";

export interface HttpAnySendReceiveOptions {
    assumeHttp2?: boolean
    ca?: string,
    cert?: string,
    key?: string,
    lookup?: net.LookupFunction | undefined;
    pingInterval?: number,
    connectTimeout?: number,
    idleTimeout?: number,
    linger?: number,
    signal?: AbortSignal
    onError: (error: unknown) => void
}

export function createHttpAnySendReceive(
    options: HttpAnySendReceiveOptions,
): HttpSendReceive {
    const {
        assumeHttp2,
        ca, cert, key,
        lookup,
        pingInterval,
        connectTimeout, idleTimeout,
        linger,
        signal,
        onError,
    } = options;

    const clientMemoizer = createMemoizer();

    return httpAnySendReceive;

    async function httpAnySendReceive(
        sendRequest: HttpSendRequest,
    ): Promise<HttpReceiveResponse> {
        const client = clientMemoizer.acquire(new URL(sendRequest.url.origin));
        try {
            const receiveResponse = await client.fetch(sendRequest);
            return receiveResponse;
        }
        finally {
            clientMemoizer.release(client, linger);
        }
    }

    function createMemoizer() {
        const memoizer = new InstanceMemoizer(
            (origin: URL) => new AnyHttpClient({
                origin,
                assumeHttp2,
                ca, cert, key,
                lookup,
                pingInterval,
                connectTimeout, idleTimeout,
                onError,
            }),
            instance => instance.destroy(),
            url => url.origin,
        );
        signal?.addEventListener(
            "abort",
            () => memoizer.flush(),
            { once: true },
        );
        return memoizer;
    }

}
