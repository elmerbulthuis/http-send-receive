import assert from "assert";
import delay from "delay";
import * as dns from "dns";
import * as fs from "fs";
import * as http from "http";
import * as http2 from "http2";
import * as https from "https";
import { second } from "msecs";
import test from "tape-promise/tape.js";
import { HttpSendReceive } from "../send-receive.js";
import { ConnectTimeoutError, IdleTimeoutError, withAbort } from "../utils/index.js";
import { createHttpAgentSendReceive } from "./send-receive-agent.js";
import { createHttpAnySendReceive } from "./send-receive-http-any.js";

const clients = [
    "agent http",
    "agent https",
    "http",
    "https",
    "h2c",
    "h2",
];

for (const client of clients) test(
    `${client} should timeout on connect timeout`,
    t => withAbort(async signal => {
        let url: URL;
        let sendReceive: HttpSendReceive;

        switch (client) {
            case "agent http":
            case "http":
            case "h2c":
                url = new URL("http://localhost:8080");
                break;

            case "agent https":
            case "https":
            case "h2":
                url = new URL("https://localhost:8080");
                break;

            default: assert.fail();
        }

        switch (client) {
            case "agent http": {
                const agent = new http.Agent({
                    keepAlive: true,
                    lookup(hostname, options, callback) {
                        setTimeout(
                            () => dns.lookup(hostname, options, callback),
                            5 * second,
                        );
                    },
                });
                sendReceive = createHttpAgentSendReceive({
                    agent,
                    connectTimeout: 4 * second,
                    idleTimeout: 2 * second,
                });
                signal.addEventListener("abort", () => {
                    agent.destroy();
                });
                break;
            }

            case "agent https": {
                const agent = new https.Agent({
                    keepAlive: true,
                    ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                    cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                    key: fs.readFileSync("./tls/key.pem", "utf8"),
                    lookup(hostname, options, callback) {
                        setTimeout(
                            () => dns.lookup(hostname, options, callback),
                            5 * second,
                        );
                    },
                });
                sendReceive = createHttpAgentSendReceive({
                    agent,
                    connectTimeout: 4 * second,
                    idleTimeout: 2 * second,
                });
                signal.addEventListener("abort", () => {
                    agent.destroy();
                });
                break;
            }

            case "http":
            case "https":
            case "h2c":
            case "h2": {
                sendReceive = createHttpAnySendReceive({
                    assumeHttp2: client === "h2c",
                    ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                    cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                    key: fs.readFileSync("./tls/key.pem", "utf8"),
                    connectTimeout: 4 * second,
                    idleTimeout: 2 * second,
                    lookup(hostname, options, callback) {
                        setTimeout(
                            () => dns.lookup(hostname, options, callback),
                            5 * second,
                        );
                    },
                    onError(error) {
                        t.comment(String(error));
                    },
                });
                break;
            }

            default: assert.fail();
        }

        try {
            const result = await sendReceive({
                headers: {},
                method: "GET",
                url,
            });

            for await (const chunk of result.body()) {
                t.comment(chunk.toString());
            }

            t.fail("should throw");
        }
        catch (error: unknown) {
            t.ok(error instanceof ConnectTimeoutError, "connect timeout error");
        }

    }),
);

for (const client of clients) test(
    `${client} should timeout on idle timeout`,
    // eslint-disable-next-line complexity
    t => withAbort(async signal => {
        let url: URL;
        let sendReceive: HttpSendReceive;
        let server: http.Server | https.Server | http2.Http2Server | http2.Http2SecureServer;

        switch (client) {
            case "agent http":
            case "http":
            case "h2c":
                url = new URL("http://localhost:8080");
                break;

            case "agent https":
            case "https":
            case "h2":
                url = new URL("https://localhost:8080");
                break;

            default: assert.fail();
        }

        switch (client) {
            case "agent http": {
                const agent = new http.Agent({
                    keepAlive: true,
                });
                sendReceive = createHttpAgentSendReceive({
                    agent,
                    connectTimeout: 1 * second,
                    idleTimeout: 2 * second,
                });

                signal.addEventListener("abort", () => {
                    agent.destroy();
                });
                break;
            }

            case "agent https": {
                const agent = new https.Agent({
                    keepAlive: true,
                    ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                    cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                    key: fs.readFileSync("./tls/key.pem", "utf8"),
                });
                sendReceive = createHttpAgentSendReceive({
                    agent,
                    connectTimeout: 1 * second,
                    idleTimeout: 2 * second,
                });

                signal.addEventListener("abort", () => {
                    agent.destroy();
                });
                break;
            }

            case "http":
            case "https":
            case "h2c":
            case "h2": {
                sendReceive = createHttpAnySendReceive({
                    assumeHttp2: client === "h2c",
                    ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                    cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                    key: fs.readFileSync("./tls/key.pem", "utf8"),
                    connectTimeout: 1 * second,
                    idleTimeout: 2 * second,
                    onError(error) {
                        t.comment(String(error));
                    },
                });
                break;
            }

            default: assert.fail();
        }

        switch (client) {
            case "agent http":
            case "http": {
                server = http.createServer(
                    {},
                    async (req, res) => {
                        res.write("1\n");
                        await delay(wait);

                        res.write("2\n");
                        await delay(wait);

                        res.write("3\n");
                        res.end();
                    },
                );
                break;
            }

            case "agent https":
            case "https": {
                server = https.createServer(
                    {
                        cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                        key: fs.readFileSync("./tls/key.pem", "utf8"),
                        ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                    },
                    async (req, res) => {
                        res.write("1\n");
                        await delay(wait);

                        res.write("2\n");
                        await delay(wait);

                        res.write("3\n");
                        res.end();
                    },
                );
                break;
            }

            case "h2c": {
                server = http2.createServer(
                    {
                    },
                    async (req, res) => {
                        res.write("1\n");
                        await delay(wait);

                        res.write("2\n");
                        await delay(wait);

                        res.write("3\n");
                        res.end();
                    },
                );
                break;
            }

            case "h2": {
                server = http2.createSecureServer(
                    {
                        cert: fs.readFileSync("./tls/cert.pem", "utf8"),
                        key: fs.readFileSync("./tls/key.pem", "utf8"),
                        ca: fs.readFileSync("./tls/ca.pem", "utf8"),
                    },
                    async (req, res) => {
                        res.write("1\n");
                        await delay(wait);

                        res.write("2\n");
                        await delay(wait);

                        res.write("3\n");
                        res.end();
                    },
                );
                break;
            }

            default: assert.fail();
        }

        let wait: number;

        try {
            await new Promise<void>(resolve => server.listen(
                Number(url.port),
                () => resolve(),
            ));

            try {
                wait = 1 * second;

                const result = await sendReceive({
                    headers: {},
                    method: "GET",
                    url,
                });

                for await (const chunk of result.body()) {
                    t.comment(chunk.toString());
                }

                t.pass("no error");
            }
            catch (error) {
                t.error(error);
            }

            try {
                wait = 3 * second;

                const result = await sendReceive({
                    headers: {},
                    method: "GET",
                    url,
                });
                for await (const chunk of result.body()) {
                    t.comment(chunk.toString());
                }

                t.fail("should throw");
            }
            catch (error) {
                t.ok(error instanceof IdleTimeoutError, "idle timeout error");
            }

        }
        finally {
            await new Promise<void>((resolve, reject) => server.close(
                error => error ?
                    reject(error) :
                    resolve(),
            ));
        }

    }),
);
