import { EventEmitter } from "events";

export function waitForEvent<T>(
    signal: AbortSignal,
    emitter: EventEmitter,
    type: string,
) {
    return new Promise<T>(resolve => {
        signal.addEventListener(
            "abort",
            () => emitter.removeListener(type, resolve),
            { once: true },
        );
        emitter.addListener(type, resolve);
    });
}

export function waitForError(
    signal: AbortSignal,
    emitter: EventEmitter,
    type = "error",
) {
    return new Promise<never>((resolve, reject) => {
        signal.addEventListener(
            "abort",
            () => emitter.removeListener(type, reject),
            { once: true },
        );
        emitter.addListener(type, reject);
    });
}

export function bindEvent(
    signal: AbortSignal,
    emitter: EventEmitter,
    type: string,
    handler: (...args: unknown[]) => void,
) {
    emitter.addListener(type, handler);
    signal.addEventListener(
        "abort",
        () => emitter.removeListener(type, handler),
        { once: true },
    );
}
