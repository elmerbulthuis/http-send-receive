import { HttpSendReceiveBodyFactory } from "./body.js";
import { HttpSendReceiveHeaders } from "./headers.js";

export interface HttpSendRequest {
    method: string;
    url: URL;
    headers: HttpSendReceiveHeaders;
    body?: HttpSendReceiveBodyFactory;
}
