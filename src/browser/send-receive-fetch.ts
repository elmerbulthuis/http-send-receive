import { HttpSendReceiveHeaders } from "../headers.js";
import { HttpReceiveResponse } from "../receive-response.js";
import { HttpSendRequest } from "../send-request.js";

export async function httpFetchSendReceive(
    sendRequest: HttpSendRequest,
) {
    const abortController = new AbortController();

    const req: RequestInit = {
        method: sendRequest.method,
        headers: Array.from(toFetchHeaders(sendRequest.headers)),
        body: sendRequest.body && await toFetchBody(sendRequest.body()),
        redirect: "manual",
        signal: abortController.signal,
        cache: "no-store",
    };

    const res = await fetch(
        sendRequest.url.toString(),
        req,
    );

    const receiveResponse: HttpReceiveResponse = {
        status: res.status,
        headers: fromFetchHeaders(res.headers),
        body: signal => {
            if (signal?.aborted) abortController.abort();
            else signal?.addEventListener("abort", () => abortController.abort());

            if (res.body == null) {
                throw new TypeError("expected body");
            }
            return fromFetchBody(res.body);
        },
    };

    return receiveResponse;
}

function* toFetchHeaders(headers: HttpSendReceiveHeaders): Iterable<[string, string]> {
    for (const [name, values] of Object.entries(headers)) {
        if (Array.isArray(values)) {
            for (const value of values) {
                yield [name, value];
            }
        }
        else {
            yield [name, values];
        }
    }
}

function fromFetchHeaders(fetchHeaders: Iterable<string[]>): HttpSendReceiveHeaders {
    const headers: HttpSendReceiveHeaders = {};
    for (const [name, value] of fetchHeaders) {
        if (name in headers) {
            const currentValues = headers[name];
            headers[name] = Array.isArray(currentValues) ?
                [...currentValues, value] :
                [currentValues, value];
        }
        else {
            headers[name] = value;
        }
    }
    return headers;
}

async function toFetchBody(
    iterable: AsyncIterable<Uint8Array>,
) {
    /*
    unfortunately, today browsers don't really support streaming uploads yet!
    This will change in the future.
    */
    const collected = new Array<number>();
    for await (const chunk of iterable) {
        collected.push(...chunk);
    }
    if (collected.length === 0) return null;

    return Uint8Array.from(collected).buffer;
}

async function* fromFetchBody(
    stream: ReadableStream<Uint8Array>,
) {
    /*
    read a readable stream as an AsyncIterable<Uint8Array>
    */
    const reader = stream.getReader();
    try {
        let result = await reader.read();
        while (!result.done) {
            yield result.value;
            result = await reader.read();
        }
    }
    finally {
        reader.releaseLock();
    }
}
