import delay from "delay";
import { second } from "msecs";
import * as path from "path";
import test from "tape-promise/tape.js";
import { BrowserContext, withBrowserContext, withEchoServerContext, withModuleServerContext } from "../testing/index.js";
import * as lib from "./send-receive-fetch.js";

const browsers = [
    "chrome",
    "firefox",
];

for (const browser of browsers) test(
    `${browser} browser should error when receiving data and killing server`,
    t => withContext({ browser }, async context => {
        const baseUrl = await context.startServer();
        const stopPromise = (async () => {
            await delay(2 * second);
            await context.stopServer();
        })();

        try {
            const okPromise = context.driver.executeAsyncScript<boolean>(
                async function (
                    this: { lib: typeof lib },
                    baseUrl: string,
                    callback: (value?: boolean) => void,
                ) {
                    let ok = true;

                    try {
                        const response = await lib.httpFetchSendReceive({
                            method: "POST",
                            url: new URL(baseUrl),
                            headers: {},
                            async *body(signal) {
                                const encoder = new TextEncoder();
                                yield encoder.encode("1\n");
                                yield encoder.encode("2\n");
                                yield encoder.encode("3\n");
                                yield encoder.encode("4\n");
                            },
                        });

                        const readPromise = (async () => {
                            for await (const chunk of response.body()) {
                                console.log(chunk.toString());
                            }
                        })();

                        await readPromise;
                        ok &&= false;
                    }
                    catch (error) {
                        ok &&=
                            error != null &&
                            typeof error === "object" &&
                            "name" in error &&
                            error.name === "TypeError";
                    }

                    callback(ok);
                },
                baseUrl.toString(),
            );

            const ok = await okPromise;

            t.ok(ok);
        }
        finally {
            await stopPromise;
        }

    }),
);

for (const browser of browsers) test(
    `${browser} browser should error when aborting request`,
    t => withContext({ browser }, async context => {
        const baseUrl = await context.startServer();
        try {
            const ok = await context.driver.executeAsyncScript<boolean>(
                async function (
                    this: { lib: typeof lib },
                    baseUrl: string,
                    callback: (value?: boolean) => void,
                ) {
                    let ok = true;

                    const abortController = new AbortController();
                    setTimeout(() => abortController.abort(), 2 * 1000);

                    const response = await lib.httpFetchSendReceive(
                        {
                            method: "POST",
                            url: new URL(baseUrl),
                            headers: {},
                            async *body() {
                                const encoder = new TextEncoder();
                                yield encoder.encode("1\n");
                                yield encoder.encode("2\n");
                                yield encoder.encode("3\n");
                                yield encoder.encode("4\n");
                            },
                        },
                    );

                    try {
                        const body = response.body(abortController.signal);
                        for await (const chunk of body) {
                            console.log(chunk.toString());
                        }
                        ok &&= false;
                    }
                    catch (error) {
                        ok &&= abortController.signal.aborted;
                    }

                    callback(ok);
                },
                baseUrl.toString(),
            );

            t.ok(ok);
        }
        finally {
            await context.stopServer();
        }
    }),
);

interface ContextOptions {
    browser: string;
}

interface Context {
    driver: BrowserContext["driver"]
    startServer(): Promise<URL>;
    stopServer(): Promise<void>;
}
async function withContext<T = void>(options: ContextOptions, job: (
    context: Context,
) => Promise<T>) {
    const dirname = path.dirname(new URL(import.meta.url).pathname);

    return withEchoServerContext(
        "http",
        ({ startServer, stopServer }) => withModuleServerContext<T>(
            {
                alias: "lib",
                input: path.resolve(dirname, "..", "main-browser.js"),

            },
            ({ url }) => withBrowserContext<T>({ browser: options.browser }, async ({
                driver,
            }) => {
                await driver.get(url.toString());

                return job({
                    driver,
                    startServer,
                    stopServer,
                });
            }),

        ),
    );
}
